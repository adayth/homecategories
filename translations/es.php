<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{homecategories}prestashop>homecategories_8d28bd813ca0aacfc0e756fd5c5e1c6a'] = 'Mostrar categorías en la página inicial';
$_MODULE['<{homecategories}prestashop>homecategories_9e677a5e287a1c61182180103bf1c580'] = 'Muestra categorías en la columna central de tu página inicial.';
$_MODULE['<{homecategories}prestashop>homefeatured_8c2754c7c761e9f41e5fc39385138884'] = 'No se encontraron categorías';
$_MODULE['<{homecategories}prestashop>tab_af1b98adf7f686b84cd0b443e022b7a0'] = 'Categorías';
