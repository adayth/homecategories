<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class HomeCategories extends Module
{
	public function __construct()
	{
		$this->name = 'homecategories';
		$this->tab = 'front_office_features';
		$this->version = '0.1';
		$this->author = 'Aday Talavera Hierro';
		$this->need_instance = 0;

		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Show categories on the homepage');
		$this->description = $this->l('Displays categories in the central column of your homepage.');
	}

	public function install()
	{
		Configuration::updateValue('HOME_CATEGORIES_DEPTH', 1);

		if (!parent::install()
            || !$this->registerHook('header')
			|| !$this->registerHook('displayHomeTab')
			|| !$this->registerHook('displayHomeTabContent')
		)
			return false;

		return true;
	}

	public function uninstall()
	{
		return parent::uninstall();
	}

	public function getContent()
	{
		$output = '';

		/*$errors = array();
		if (Tools::isSubmit('submitHomeCategories'))
		{
			$nbr = (int)Tools::getValue('HOME_CATEGORIES_DEPTH');
			if (!$nbr || $nbr <= 0 || !Validate::isInt($nbr))
				$errors[] = $this->l('An invalid number has been specified.');
			else
				Configuration::updateValue('HOME_CATEGORIES_DEPTH', (int)$nbr);
			if (isset($errors) && count($errors))
				$output .= $this->displayError(implode('<br />', $errors));
			else
				$output .= $this->displayConfirmation($this->l('Your settings have been updated.'));
		}
        $output .= $this->renderForm();*/

		return $output;
	}

    public function hookHeader($params) {
        $this->context->controller->addCSS($this->_path.'css/homecategories.css', 'all');
    }

    public function hookDisplayHomeTab($params)
	{
		return $this->display(__FILE__, 'tab.tpl');
	}

	public function hookDisplayHomeTabContent($params)
	{
        $id_category = Configuration::get('PS_HOME_CATEGORY');
        // Instantiate category
        $this->category = new Category($id_category, $this->context->language->id);
        if (!$this->category->active || !$this->category->checkAccess($this->context->customer->id)) {
            return;
        }
        if ($subCategories = $this->category->getSubCategories($this->context->language->id)) {
            $this->context->smarty->assign(array(
                'subcategories' => $subCategories,
                'subcategories_nb_total' => count($subCategories),
                'subcategories_nb_half' => ceil(count($subCategories) / 2)
            ));
        }
        return $this->display(__FILE__, 'homefeatured.tpl');
    }

    public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Categories depth to show'),
						'name' => 'HOME_CATEGORIES_DEPTH',
						'class' => 'fixed-width-xs',
						'desc' => $this->l('Set the categories depth that you would like to display on homepage (default: 1).'),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->id = (int)Tools::getValue('id_carrier');
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitHomeCategories';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		return array(
			'HOME_CATEGORIES_DEPTH' => Tools::getValue('HOME_CATEGORIES_DEPTH', Configuration::get('HOME_CATEGORIES_DEPTH')),
		);
	}
}
